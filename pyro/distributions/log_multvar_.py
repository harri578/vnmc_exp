import torch
from torch.distributions import constraints
from torch.distributions.transforms import SigmoidTransform, ExpTransform

from pyro.distributions.torch import MultivariateNormal, TransformedDistribution
from pyro.distributions.torch_distribution import TorchDistribution
from pyro.util import is_bad

import matplotlib.pyplot as plt
import math

class LogMultivariateNormal_(TorchDistribution):
    
    arg_constraints = {'loc': constraints.real_vector,
                       # 'covariance_matrix': constraints.positive_definite,
                       # 'precision_matrix': constraints.positive_definite,
                       'scale_tril': constraints.lower_cholesky}
    support = constraints.real_vector
    has_rsample = True


    def __init__(self, loc, covariance_matrix=None, precision_matrix=None, scale_tril=None, validate_args=None):
        # import pdb
        # pdb.set_trace()
        self.transform = ExpTransform()
        self.multvarnorm = MultivariateNormal(loc = loc, scale_tril = scale_tril)
        self.loc = loc
        self.scale_tril = scale_tril
        self.base_dist = TransformedDistribution(self.multvarnorm, [self.transform])    
        # import pdb
        # pdb.set_trace()
        # assert self.base_dist.event_dim == 0 or self.base_dist.event_shape[0] == 1
        # assert self.base_dist.event_dim == 0 or self.base_dist.event_dim == 1 and self.base_dist.event_shape[0] == 1
        super(LogMultivariateNormal_, self).__init__(self.base_dist.batch_shape, self.base_dist.event_shape,
                                                    validate_args=validate_args)

    def expand(self, batch_shape, _instance=None):
        # import pdb
        # pdb.set_trace()
        new = self._get_checked_instance(LogNormal_, _instance)
        batch_shape = torch.Size(batch_shape)
        new.loc = self.loc
        new.scale_tiril = self.scale_tril
        new.transform = self.transform
        new.base_dist = self.base_dist.expand(batch_shape)
        super(LogNormal_, new).__init__(batch_shape, validate_args=False)
        new._validate_args = self._validate_args
        return new

    # def z(self, value):
    #     return (self.transform.inv(value) - self.base_dist.base_dist.loc) / self.base_dist.base_dist.scale

    @constraints.dependent_property
    def support(self):
        raise NotImplementedError

    def sample(self, sample_shape=torch.Size()):
        import pdb
        pdb.set_trace()
        with torch.no_grad():
            x = self.base_dist.sample(sample_shape)
            # x[x > self.upper_lim] = self.upper_lim
            # x[x < self.lower_lim] = self.lower_lim
            return x

    def rsample(self, sample_shape=torch.Size()):
        # import pdb
        # pdb.set_trace()
        x = self.base_dist.sample(sample_shape)
        # x[x > self.upper_lim] = self.upper_lim
        # x[x < self.lower_lim] = self.lower_lim
        return x

    def log_prob(self, value):
        """
        Scores the sample by giving a probability density relative to a new base measure.
        The new base measure places an atom at `self.upper_lim` and `self.lower_lim`, and
        has Lebesgue measure on the intervening interval.

        Thus, `log_prob(self.lower_lim)` and `log_prob(self.upper_lim)` represent probabilities
        as for discrete distributions. `log_prob(x)` in the interior represent regular
        pdfs with respect to Lebesgue measure on R.

        **Note**: `log_prob` scores from distributions with different censoring are not
        comparable.
        """
        # import pdb
        # pdb.set_trace()
        log_prob = self.base_dist.log_prob(value)

        checking = MultivariateNormal(loc = self.loc, scale_tril = self.scale_tril)
        log_prob1 = checking.log_prob(torch.log(value))
        # crit = 1e-40
        # log_prob[torch.isclose(value, self.upper_lim,rtol=1e-6)] = 
        # log_prob[value > self.upper_lim] = float('-inf')
        # log_prob[torch.isclose(value, self.lower_lim,rtol=1e-6)] = lower_cdf.expand_as(log_prob)[torch.isclose(value, self.lower_lim,rtol=1e-6)]
        # log_prob[value < self.lower_lim] = float('-inf')
        # import pdb
        # pdb.set_trace()
        # To compute the log cdf, we use log(cdf), except where it would give -inf
        # In those cases we use an asymptotic formula log_prob(value) - value.abs().log(
        return log_prob1
        # return torch.sum(log_prob,-1)

    def cdf(self, value):
        import pdb
        pdb.set_trace()
        if self._validate_args:
            self.base_dist._validate_sample(value)
        cdf = self.base_dist.cdf(value)
        # cdf[value >= self.upper_lim] = 1.
        # cdf[value < self.lower_lim] = 0.