import torch
from torch.distributions import constraints
from torch.distributions.transforms import SigmoidTransform, ExpTransform

from pyro.distributions.torch import Normal, TransformedDistribution
from pyro.distributions.torch_distribution import TorchDistribution
from pyro.util import is_bad

import matplotlib.pyplot as plt
import math
from scipy import stats
import numpy as np

class LogNormal_2(TorchDistribution):
    
    arg_constraints = {'loc': constraints.real, 'scale': constraints.positive}
    support = constraints.positive
    has_rsample = True

    def __init__(self, loc, scale, validate_args=None):
        self.normal = Normal(loc, scale, validate_args=validate_args)
        self.loc = loc
        self.scale = scale
        self.transform = ExpTransform()
        self.base_dist = TransformedDistribution(self.normal, [self.transform])
        # Log-prob only computed correctly for univariate base distribution
        assert self.base_dist.event_dim == 0 or self.base_dist.event_dim == 1 and self.base_dist.event_shape[0] == 1
        super(LogNormal_2, self).__init__(self.base_dist.batch_shape, self.base_dist.event_shape,
                                                    validate_args=validate_args)
        # self.upper_lim = upper_lim
        # self.lower_lim = lower_lim

    def expand(self, batch_shape, _instance=None):
        # import pdb
        # pdb.set_trace()
        new = self._get_checked_instance(LogNormal_, _instance)
        batch_shape = torch.Size(batch_shape)
        # new.upper_lim = self.upper_lim
        # new.lower_lim = self.lower_lim
        new.transform = self.transform
        new.base_dist = self.base_dist.expand(batch_shape)
        new.loc = self.loc
        new.scale = self.scale
        new.normal = self.normal
        super(LogNormal_, new).__init__(batch_shape, validate_args=False)
        new._validate_args = self._validate_args
        return new

    # def z(self, value):
    #     return (self.transform.inv(value) - self.base_dist.base_dist.loc) / self.base_dist.base_dist.scale

    @constraints.dependent_property
    def support(self):
        raise NotImplementedError

    def sample(self, sample_shape=torch.Size()):
        with torch.no_grad():
            x = self.base_dist.sample(sample_shape)
            # x[x > self.upper_lim] = self.upper_lim
            # x[x < self.lower_lim] = self.lower_lim
            return x

    def rsample(self, sample_shape=torch.Size()):
        # import pdb
        # pdb.set_trace()
        x = self.base_dist.sample(sample_shape)
        # x[x > self.upper_lim] = self.upper_lim
        # x[x < self.lower_lim] = self.lower_lim
        return x

    def log_prob(self, value):
        """
        Scores the sample by giving a probability density relative to a new base measure.
        The new base measure places an atom at `self.upper_lim` and `self.lower_lim`, and
        has Lebesgue measure on the intervening interval.

        Thus, `log_prob(self.lower_lim)` and `log_prob(self.upper_lim)` represent probabilities
        as for discrete distributions. `log_prob(x)` in the interior represent regular
        pdfs with respect to Lebesgue measure on R.

        **Note**: `log_prob` scores from distributions with different censoring are not
        comparable.
        """
        # import pdb
        # pdb.set_trace()
        # rv1 = stats.norm(loc = np.log(1), scale = np.sqrt(0.05))
        # rv2 = stats.norm(loc = np.log(0.1), scale = np.sqrt(0.05))
        # rv3 = stats.norm(loc = np.log(20), scale = np.sqrt(0.05))    
        # value1 = torch.log(value)
        # p1_lp = rv1.logpdf(value1[...,0])
        # p2_lp = rv2.logpdf(value1[...,1])
        # p3_lp = rv3.logpdf(value1[...,2])

        # log_prob1 = p1_lp + p2_lp + p3_lp

        log_prob = torch.sum(self.normal.log_prob(torch.log(value)),-1)

        # crit = 1e-40
        # log_prob[torch.isclose(value, self.upper_lim,rtol=1e-6)] = 
        # log_prob[value > self.upper_lim] = float('-inf')
        # log_prob[torch.isclose(value, self.lower_lim,rtol=1e-6)] = lower_cdf.expand_as(log_prob)[torch.isclose(value, self.lower_lim,rtol=1e-6)]
        # log_prob[value < self.lower_lim] = float('-inf')
        # To compute the log cdf, we use log(cdf), except where it would give -inf
        # In those cases we use an asymptotic formula log_prob(value) - value.abs().log(
        return log_prob
        # return torch.sum(log_prob,-1)

    def cdf(self, value):
        import pdb
        pdb.set_trace()
        if self._validate_args:
            self.base_dist._validate_sample(value)
        cdf = self.base_dist.cdf(value)
        # cdf[value >= self.upper_lim] = 1.
        # cdf[value < self.lower_lim] = 0.