# Code for 'Variational Bayesian Optimal Experimental Design'

### Installation
We use three repo. you can follow the instructions how to install each repo. When you use install MINE, you can install it with 
    > pip install -e .

### AB test(VNMC environment)
The following reproduces the REIG estimation for the A/B test.

    > python3 examples/contrib/oed/reig_estimation_benchmarking.py --case-tags=ab_test --seed=<seed> --name=<your filename without extension>

### Preference(VNNMC environemt)
The following reproduces the REIG estimation for the preference test.

    > python3 examples/contrib/oed/reig_estimation_benchmarking.py --case-tags=preference --seed=<seed> --name=<your filename without extension>

### Pharacokinetic model (ACE environment)
The following reproduces the REIG estimation for the preference test.

    > python3 examples/contrib/oed/reig_estimation_benchmarking.py --case-tags=preference --seed=<seed> --name=<your filename without extension>

### MINEBED environment
If you want to check the MINE estimator, you can run the experiments in the MINEBED environment. 